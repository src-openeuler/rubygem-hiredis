%global gem_name hiredis

Name: rubygem-%{gem_name}
Version: 0.6.3
Release: 1
Summary: Ruby wrapper for hiredis
License: BSD
URL: http://github.com/redis/hiredis-rb
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
# Get the test suite:
# git clone https://github.com/redis/hiredis-rb.git && cd hiredis-rb/
# git checkout v0.6.3 && tar czvf hiredis-0.6.3-tests.tgz test/
Source1: %{gem_name}-%{version}-tests.tgz
# Build against system hiredis library
Patch0: rubygem-hiredis-0.6.1-Build-against-system-hiredis.patch
# Compatibility with hiredis 1.0.0.
# https://github.com/redis/hiredis-rb/pull/69
Patch1: hiredis-0.6.3-bump-hiredis-to-1.0.0.patch
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby-devel
BuildRequires: gcc
BuildRequires: hiredis-devel
BuildRequires: rubygem(minitest)

%description
Ruby wrapper for hiredis (protocol serialization/deserialization and blocking
I/O).

%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version} -b1


# Remove bundled hiredis
%gemspec_remove_file Dir.glob('vendor/**/*')
rm -rf ./vendor

# Use system hiredis
%patch0 -p1
%patch1 -p1


%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{gem_extdir_mri}/hiredis/ext
cp -a .%{gem_extdir_mri}/gem.build_complete %{buildroot}%{gem_extdir_mri}/
cp -a .%{gem_extdir_mri}/%{gem_name}/ext/*.so %{buildroot}%{gem_extdir_mri}/%{gem_name}/ext

# Prevent dangling symlink in -debuginfo (rhbz#878863).
rm -rf %{buildroot}%{gem_instdir}/ext/


%check
pushd .%{gem_instdir}
cp -a %{_builddir}/test .

# Tests calling #flush don't work properly.
# https://github.com/redis/hiredis-rb/issues/62
sed -i '/^  def test_recover_from_partial_write/ a skip' \
  test/connection_test.rb
sed -i '/^  def test_eagain_on_write_followed_by_remote_drain/ a skip' \
  test/connection_test.rb

# Make sure the test does not fail in mock with disabled networking.
sed -i -r '/(name or service not known)/ s|(/i)|\|(temporary failure in name resolution)\1|' \
  test/connection_test.rb

ruby -Ilib:$(dirs +1)%{gem_extdir_mri} -e 'Dir.glob "./test/**/*_test.rb", &method(:require)'
popd

%files
%dir %{gem_instdir}
%{gem_extdir_mri}
%license %{gem_instdir}/COPYING
%{gem_libdir}
%exclude %{gem_cache}
%exclude %{gem_instdir}/ext
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/Rakefile

%changelog
* Thu Mar 3 2022 caodongxia<caodongxia@huawei.com> - 0.6.3-1
- Init package
